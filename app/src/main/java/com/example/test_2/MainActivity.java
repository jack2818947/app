package com.example.test_2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private EditText editHeight;//創造身高物件
    private EditText editWeight;//創造體重物件
    private Button button;//創造按鈕
    private TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editHeight = findViewById(R.id.editHeight);//連接component
        editWeight = findViewById(R.id.editWeight);
        button = findViewById(R.id.btnCalculate);
        txtResult = findViewById(R.id.txtResult);

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DecimalFormat nf = new DecimalFormat("0.00");//取小數點後兩位
                //這邊寫下按下按鈕時要做的事
                double Weight = Double.parseDouble(editWeight.getText().toString());//get身體資料並轉換成string
                double Height = Double.parseDouble(editHeight.getText().toString());
                double BMI = Weight / (Height * Height);//計算BMI
                //數值顯示
                TextView result = (TextView) findViewById(R.id.txtResult);
                txtResult.setText(nf.format(BMI));
                //建議
                TextView suggest = (TextView) findViewById(R.id.suggest);
                if (BMI > 25.00) { //太重了
                    suggest.setText("過胖！！");
                } else if (BMI < 20.00) { //太輕了
                    suggest.setText("過瘦！");
                } else { //剛剛好
                    suggest.setText("適中！");
                }
            }
        });
    }
}